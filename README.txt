Hier ein Beispiel für statische Seiten analog zu git-pages.
Dieses Repository ist unter [https://bock.pages.io-warnemuende.de/](https://bock.pages.io-warnemuende.de/) zu erreichen.

13.02.2025: Test von Webhooks, um Push-Events in Rocket.Chat darzustellen.